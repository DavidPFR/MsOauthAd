<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://cdn.tailwindcss.com"></script>
  <title>Ms Login</title>
</head>

<body style="font-family:Roboto">
  <div class="w-full h-screen md:-mx-4" style="filter: blur(6px); background-image:url('https://unsplash.com/photos/KMn4VEeEPR8/download?ixid=MnwxMjA3fDB8MXxzZWFyY2h8Mnx8YmVhY2h8ZW58MHx8fHwxNjQ3MzM1MzMx&force=true&w=1920')"></div>
  <div class="absolute w-3/5 max-w-lg bg-white" style="transform: translate(-50%, -50%); top:50%; left:50%">
    <div class="flex justify-center -mt-20">
      <img class="border-2 w-40 h-40 rounded-full object-cover" src="https://unsplash.com/photos/QZRAaYfmvA8/download?ixid=MnwxMjA3fDB8MXxzZWFyY2h8MTE3fHxmYWNlJTIwZ2lybHxlbnwwfHx8fDE2NDczMzg3NjI&force=true&w=640">
    </div>
    <div class="px-12 py-10">
      <div class="w-full mb-2">
        <div class="flex items-center">
          <i class='ml-3 fill-current text-gray-400 text-xs z-10 far fa-user'></i>
          <input type='text' placeholder="username" class="-mx-6 px-8  w-full border rounded px-3 py-1 text-gray-700" />
        </div>
      </div>
      <div class="w-full mb-2">
        <div class="flex items-center">
          <i class='ml-3 fill-current text-gray-400 text-xs z-10 fas fa-lock'></i>
          <input type='text' placeholder="password" class="-mx-6 px-8 w-full border rounded px-3 py-1 text-gray-700" />
        </div>
      </div>
      <div class="mt-8 flex justify-between">
        <div class="flex items-center">
          <input type="checkbox" class="w-4 h-4 mr-2">
          <span class="text-xs text-gray-700">Remember Me</span>
        </div>
        <div>
          <a href="/signin.php">
            <button type='text' class="bg-yellow-400 text-xs text-gray-700 rounded px-4 py-2">
              Sign in with Microsoft
            </button>
          </a>
        </div>
      </div>
    </div>
  </div>
  </div>
</body>

</html>